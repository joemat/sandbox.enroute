An example project created from the osgi enroute templates

[1]: http://enroute.osgi.org/quick-start.html
[2]: http://enroute.osgi.org/tutorial_base/800-ci.html
[3]: https://www.gradle.org/


[![Build Status](https://drone.io/bitbucket.org/joemat/sandbox.enroute/status.png)](https://drone.io/bitbucket.org/joemat/sandbox.enroute/latest)